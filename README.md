---------------WEEK TWO---------------

EXERCISE

-Write a simple routine in JavaScript, that:

--Accepts a name of any animal from a user

--Checks if the name given is in a list of animals,

--Gives a response to the user, if the name is there or not


---------------WEEK THREE---------------

EXERCISE

-Initialize git in the Week 2 project, and complete the workflow to host on GitLab

-Request to add your name to the list of interns, by forking, editing and creating a Merge Request on an Upstream repository
